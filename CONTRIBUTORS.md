Warriors game contributors (sorted alphabetically)
============================================

* **[Sabrina SI HADJ MOHAND](https://gitlab.com/SabrinaSH)**

  * Initialisation and rooting of the project 
  * Default and Personnage functions developement
  * Setting necessary md files to the project
  * Taking the front in charge 
  * Producing and accepting merge requests

* **[Yanis GHLIS](https://gitlab.com/CopainYanis)**

  * Initialisation of the different sittings of the project
  * Arme and Bouclier functions developement
  * Adding the docker component to the project
  * Putting in place the php-cs-fixer 
  * producing and accepting merge requests


