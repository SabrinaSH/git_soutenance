<?php

class Arme
{
    public $name;
    public $atk_point;

    public function __construct($name, $atk_point)
    {
        $this->name = $name;
        $this->atk_point = $atk_point;
    }
}
