<?php

class personnage extends DB
{
    protected $id=null;
    protected $vie = 80;
    protected $atk = 20;
    protected $nom;
    protected $arme;


    public function __construct($nom, $arme=null)
    {
        $this->nom = $nom;

        if ($arme) {
            $this->arme = $arme;
            $this->atk += $arme->atk_point;
        }
    }

    public function crier()
    {
        echo 'LERROY JINKINZ';
    }

    public function regenerer($nbPoints = null)
    {
        if (is_null($nbPoints)) {
            $this->vie = 100;
        } else {
            $this->vie += $nbPoints;
        }
    }

    public function mort()
    {
        if ($this->vie <= 0) {
            return true;
        }
        return false;
    }

    public function attaque($cible)
    {
        $cible->vie -= $this->atk;
    }
}
