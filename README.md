# Warriors game

This mostly pure PHP programme is a war game made in order to put ourselves in real situatuions of php uses during this 3rd year WI  at ESGI school .

## prerequisites

* PHP
* DOCKER
* MySQL


## How is the programme used ? 

nothing's easier  :
* Place yourself in the folder where you want to clone the project
* Clone the project from our gitlab repo : [Git Repo](https://gitlab.com/SabrinaSH/git_soutenance)
* Once the project cloned open a terminal in it 
* Build your docker-compose using the right command  
`docke-compose up --build` for example
* Once your docker is Up you can start manipulating the code in your text Editor `VisualStudioCode` , `PhpStorm` or any other


## Authors

 *  Sabrina SI HADJ MOHAND `SabrinaSH`
 *  Yanis GHLIS `CopainYanis`



